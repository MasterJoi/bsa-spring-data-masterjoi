package com.bsa.springdata.office;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface OfficeRepository extends JpaRepository<Office, UUID> {
    @Query("SELECT o FROM Office o " +
            "JOIN o.users u " +
            "JOIN u.team t " +
            "JOIN t.project p " +
            "JOIN t.technology th " +
            "ON th.name = :technology " +
            "GROUP BY o.id")
    List<Office> findAllByTechnology(String technology);

    Office findOfficeByAddress(String address);
}
