package com.bsa.springdata.team;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class TeamService {
    @Autowired
    private TeamRepository teamRepository;

    @Transactional
    public void updateTechnology(int devsNumber, String oldTechnologyName, String newTechnologyName) {
        // TODO: You can use several queries here. Try to keep it as simple as possible
        List<Team> smallTeamsWithTechnology = teamRepository.findSmallTeamByTechnology(oldTechnologyName, devsNumber);
        for (Team team : smallTeamsWithTechnology) {
            team.getTechnology().setName(newTechnologyName);
            teamRepository.save(team);
        }
    }

    @Transactional
    public void normalizeName(String hipsters) {
        // TODO: Use a single query. You need to create a native query
        teamRepository.normalizeName(hipsters);
    }
}
