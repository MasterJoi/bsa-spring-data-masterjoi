package com.bsa.springdata.team;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface TeamRepository extends JpaRepository<Team, UUID> {
    int countByTechnologyName(String newTechnology);

    Optional<Boolean> findByName(String hipsters_facebook_javaScript);

    @Query("SELECT t FROM Team t " +
            "JOIN t.technology th " +
            "JOIN t.users u " +
            "ON th.name = :oldTechnologyName " +
            "AND u.size < :devsNumber")
    List<Team> findSmallTeamByTechnology(String oldTechnologyName, int devsNumber);

    @Modifying
    @Query(value = "UPDATE Teams t SET name = " +
            "(SELECT CONCAT(t.name, '_', p.name, '_', th.name)" +
            "FROM Projects p " +
            "JOIN Technologies th ON th.id = t.technology_id " +
            "WHERE p.id = t.project_id)" +
            "WHERE t.name = ?1", nativeQuery = true)
    void normalizeName(String hipsters);
}
