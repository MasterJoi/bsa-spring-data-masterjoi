package com.bsa.springdata.project;

import com.bsa.springdata.project.dto.ProjectSummaryDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface ProjectRepository extends JpaRepository<Project, UUID> {
    @Query("SELECT COUNT( DISTINCT p) " +
            "FROM Project p " +
            "JOIN p.teams t " +
            "JOIN t.users u " +
            "JOIN u.roles r ON r.name = :role")
    int getCountWithRole(String role);

   @Query(value = "SELECT p.name, " +
            "COUNT(DISTINCT t) AS TeamsNumber, " +
            "COUNT(DISTINCT u) AS DevelopersNumber, " +
            "array_to_string(array_agg(distinct th.name order by th.name desc), ',') AS technologies " +
            "FROM Projects p " +
            "JOIN Teams t ON p.id = t.project_id " +
            "JOIN Technologies th ON th.id = t.technology_id " +
            "JOIN Users u ON t.id = u.team_id " +
            "GROUP BY p.id ORDER BY p.name", nativeQuery = true)
    List<ProjectSummaryDto> getProjectsSummary();

   @Query(value = "SELECT p.* FROM Projects p " +
           "JOIN Teams t ON p.id = t.project_id " +
           "JOIN Technologies th ON th.id = t.technology_id " +
           "JOIN Users u ON t.id = u.team_id " +
           "WHERE th.name = ?1 " +
           "GROUP BY p.id ORDER BY COUNT(u) LIMIT 5", nativeQuery = true)
    List<Project> findTop5ByTechnology(String technology);

    @Query(value = "SELECT p.* FROM Projects p " +
            "JOIN Teams t ON p.id = t.project_id " +
            "JOIN Users u ON t.id = u.team_id " +
            "GROUP BY p.id ORDER BY COUNT(t) DESC, COUNT(u) DESC, p.name DESC LIMIT 1", nativeQuery = true)
    Project findTheBiggest();
}