package com.bsa.springdata.project;

import com.bsa.springdata.project.dto.CreateProjectRequestDto;
import com.bsa.springdata.team.Team;
import com.bsa.springdata.team.Technology;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "projects")
public class Project {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID"
            , strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id", updatable = false, nullable = false)
    private UUID id;

    @Column
    private String name;

    @Column
    private String description;

    @OneToMany(mappedBy = "project", cascade = CascadeType.ALL ,
            fetch = FetchType.LAZY)
    private List<Team> teams;

    public static Project fromCreateProjectRequestDto(CreateProjectRequestDto projectRequestDto) {
        Technology technology = Technology.builder()
                .name(projectRequestDto.getTech())
                .description(projectRequestDto.getTechDescription())
                .link(projectRequestDto.getTechLink())
                .build();

        Team team = Team
                .builder()
                .name(projectRequestDto.getTeamName())
                .room(projectRequestDto.getTeamRoom())
                .area(projectRequestDto.getTeamArea())
                .technology(technology)
                .build();

        return Project.builder()
                .name(projectRequestDto.getProjectName())
                .description(projectRequestDto.getProjectDescription())
                .teams(Collections.singletonList(team))
                .build();
    }
}

