package com.bsa.springdata.user;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {
    List<User> findAllByOffice_CityOrderByLastNameAsc(String city);

    List<User> findAllByLastNameStartingWithIgnoreCase(String lastName, Pageable pageable);

    List<User> findAllByExperienceGreaterThanEqualOrderByExperienceDesc(int experience);

    List<User> findAllByOffice_CityAndAndTeam_RoomOrderByLastNameAsc(String city, String room);

    int deleteAllByExperienceIsLessThan(int experience);
}
